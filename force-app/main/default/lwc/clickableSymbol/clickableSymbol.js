import { LightningElement, api } from 'lwc';

export default class ClickableSymbol extends LightningElement {
    @api columnValue;
    @api fieldType;
    @api iconName;
 
    handleClick(event) {
        console.log('clickableSymbol :: handleClick');
        const clickEvent = new CustomEvent('symbolclick', {
            composed: true,
            bubbles: true,
            cancelable: true,
            detail: {
                recordId: this.columnValue,
                left: event.clientX,
                top: event.clientY,
                fieldType: this.fieldType
            },
        });
        this.dispatchEvent(clickEvent);
    }
}