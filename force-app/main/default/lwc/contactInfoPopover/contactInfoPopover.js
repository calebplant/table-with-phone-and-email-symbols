import { api, LightningElement } from 'lwc';

const TOP_OFFSET = 110;
const POPOVER_OFFSET = 10;
const LEFT_OFFSET = 15;

export default class ContactInfoPopover extends LightningElement {
    @api topMargin;
    @api leftMargin;
    @api showAllContactInfo;
    @api contactInfoFields;
    @api
    get rowData() {
        return this._rowData;
    }
    set rowData(value) {
        this._rowData = value;
        if(value) {
            this.updateDisplayedRows();
        } else {
            this.displayedRows = undefined;
        }
    }

    displayedRows;
    _rowData;

    get boxClass() { 
        return `top:${this.topMargin - (TOP_OFFSET + POPOVER_OFFSET)}px; left:${this.leftMargin + LEFT_OFFSET}px; position: absolute;`;
    }

    connectedCallback() {
        console.log('ContactInfoPopover :: connectedCallback');
    }

    // Handlers
    handlePhoneClick(event) {
        console.log('ContactInfoPopup :: handlePhoneClick');
        event.preventDefault();
    }

    handleEmailClick(event) {
        console.log('ContactInfoPopup :: handleEmailClick');
        this.dispatchEvent(new CustomEvent('composeemail', {detail: event.target.name}));
    }

    handleCloseModal(event) {
        console.log('ContactInfoPopup :: handleCloseModal');
        this.dispatchEvent(new CustomEvent('closemodal'));
    }

    // Utils
    updateDisplayedRows() {
        console.log('ContactInfoPopup :: updateDisplayedRows');
        if(this.showAllContactInfo) {
            this.displayedRows = this.contactInfoFields.map(eachField => {
                return {
                    isEmail: eachField.type === 'email',
                    value: this._rowData[eachField.fieldName],
                    label: eachField.label,
                }
            });
        }
        else {
            this.displayedRows = {
                isEmail: this._rowData.fieldType === 'email',
                value: this._rowData.value,
                label: this._rowData.value,
            }
        }
    }

}