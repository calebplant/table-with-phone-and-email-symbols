export default function mockData() {
    return [
        {
          "Id": "0010R000016DWFSQA4",
          "FirstName": "Fitzgerald",
          "LastName": "Ramirez",
          "Phone": "9805503081",
          "Email": "fitzgerald.ramirez@irure.com",
          "Fax": "8934703756"
        },
        {
          "Id": "0010R000016DWFSQA5",
          "FirstName": "Kerr",
          "LastName": "Buchanan",
          "Phone": "9204103711",
          "Email": "kerr.buchanan@aliquip.com",
          "Fax": "8904403052"
        },
        {
          "Id": "0010R000016DWFSQA6",
          "FirstName": "Rogers",
          "LastName": "Brewer",
          "Phone": "8125422412",
          "Email": "rogers.brewer@ut.com",
          "Fax": "9944402246"
        },
        {
          "Id": "0010R000016DWFSQA7",
          "FirstName": "Erma",
          "LastName": "Hart",
          "Phone": "+1 (812) 555-2721",
          "Email": "erma.hart@non.com",
          "Fax": "8085712789"
        },
        {
          "Id": "0010R000016DWFSQA8",
          "FirstName": "Shelley",
          "LastName": "Merritt",
          "Phone": "9384133566",
          "Email": "shelley.merritt@non.com",
          "Fax": "9075133391"
        },
        {
          "Id": "0010R000016DWFSQA9",
          "FirstName": "Tamika",
          "LastName": "Ware",
          "Phone": "9764333598",
          "Email": "tamika.ware@dolor.com",
          "Fax": "9814032347"
        },
        {
          "Id": "0010R000016DWFSQA0",
          "FirstName": "Olivia",
          "LastName": "Carlson",
          "Phone": "8315052717",
          "Email": "olivia.carlson@quis.com",
          "Fax": "19194852278"
        },
        {
            "Id": "0010R000016DWFSQA0",
            "FirstName": "Caleb",
            "LastName": "Plant",
            "Phone": "6128324792",
            "Email": "calebplant@aggienetwork.com",
            "Fax": "41648956895"
          }
      ]
}