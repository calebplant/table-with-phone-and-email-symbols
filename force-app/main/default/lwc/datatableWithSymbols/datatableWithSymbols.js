import { api, LightningElement } from 'lwc';
import getMockData from './mockData';

const BUTTON_WIDTH = 90;

export default class DatatableWithSymbols extends LightningElement {
    @api objectName = 'Contact';
    @api showAllContactInfo = false;
    @api records;
    @api fields = [
        {label: 'Last Name', fieldName: 'LastName', type: 'text'},
        {label: 'First Name', fieldName: 'FirstName', type: 'text'},
        {label: 'Phone', fieldName: 'Phone', type: 'phone'},
        {label: 'Email', fieldName: 'Email', type: 'email'},
        {label: 'Fax', fieldName: 'Fax', type: 'phone'},
    ];

    selectedRowData;
    selectedEmail;
    contactInfoFields;
    top;
    left;
    showPopover=false;

    connectedCallback() {
        console.log('DatatableWithSymbols :: connectedCallback');
        this.records = getMockData();
        this.contactInfoFields = this.fields.filter(eachField => eachField.type === 'phone' || eachField.type === 'email')
    }

    get displayedColumns() {
        console.log('DatatableWithSymbols :: get displayedColumns');
        let result = this.fields.map(eachField => {
            if(eachField['type'] === 'phone' || eachField['type'] === 'email') {
                let iconName;
                switch(eachField['type']) {
                    case 'phone':
                        iconName = 'utility:call';
                        break;
                    case 'email':
                        iconName = 'utility:email';
                        break;
                }
                return {
                    label: eachField.label,
                    fieldName: eachField.fieldName,
                    type: 'clickableSymbol',
                    initialWidth: BUTTON_WIDTH,
                    typeAttributes: {
                        iconName: iconName,
                        fieldType: eachField.type
                    }
                }
            }
            else {
                return eachField;
            }
        });
        console.log(JSON.parse(JSON.stringify(result)));
        return result;
    }

    // Handlers
    handleSymbolClick(event) {
        console.log('DatatableWithSymbols :: handleSymbolClick');
        let recordId = event.detail.recordId;
        let fieldType = event.detail.fieldType;
        this.selectedRowData = this.showAllContactInfo ? 
            this.records.find(item => item.Id == recordId) : {
                value: recordId,
                fieldType: fieldType
            };
        this.left = event.detail.left;
        this.top = event.detail.top;
        this.showPopover = true;
    }

    handleEmailClicked(event) {
        console.log('DatatableWithSymbols :: handleEmailClicked');
        this.showPopover = false;
        this.selectedRowData = undefined;
        this.selectedEmail = event.detail;
        this.showComposeEmailPopup = true;
    }

    handleClose(event) {
        console.log('DatatableWithSymbols :: handleClose');
        let source = event.target.name;
        switch (source) {
            case 'sendEmailModal':
                this.showComposeEmailPopup = false;
                this.showPopover = false;
                break;
            case 'contactInfoModal':
                this.showPopover = false;
                break;
        }
        this.selectedRowData = undefined;
        this.selectedEmail = undefined;
    }
}