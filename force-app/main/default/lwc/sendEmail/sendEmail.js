import { api, LightningElement } from 'lwc';
import { ShowToastEvent } from 'lightning/platformShowToastEvent';
import sendEmail from '@salesforce/apex/EmailUtils.sendEmails';

export default class SendEmail extends LightningElement {
    @api targetEmail;
    
    subject;
    body;

    get disableSend() {
        return !(this.targetEmail && this.subject && this.body);
    }

    // Handlers
    handleChange(event) {
        console.log('SendEmail :: handleChange');
        let source = event.target.name;
        switch (source) {
            case 'recipient':
                this.targetEmail = event.detail.value;
                break;
            case 'subject':
                this.subject = event.detail.value;
                break;
            case 'body':
                this.body = event.detail.value;
                break;
        }
    }

    handleSend(event) {
        console.log('SendEmail :: handleSend');
        let payload = {
            recipients: [this.targetEmail],
            subject: this.subject,
            body: this.body
        }

        sendEmail(payload)
        .then(res => {
            console.log('Successfully sent email');
            this.dispatchEvent(new ShowToastEvent({
                title: 'Success',
                message: 'Your email was sent.',
                variant: 'success'
            }));
        })
        .catch(err => {
            console.error(err);
            this.dispatchEvent(new ShowToastEvent({
                title: 'Error Sending Email',
                message: err,
                variant: 'error'
            }));
        })
        
    }

    handleCloseModal(event) {
        console.log('SendEmail :: handleCloseModal');
        this.dispatchEvent(new CustomEvent('closemodal'));
    }
}