import LightningDatatable from 'lightning/datatable';
import clickableSymbol from './clickableSymbol';
 
export default class CustomDatatable extends LightningDatatable {
    static customTypes = {
        clickableSymbol: {
            template: clickableSymbol,
            typeAttributes: ['columnValue', 'iconName', 'fieldType']
        }
    }
}