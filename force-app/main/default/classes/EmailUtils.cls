public with sharing class EmailUtils {

    @AuraEnabled
    public static void sendEmails(List<String> recipients, String subject, String body) {
        System.debug('START sendEmails');

        List<Messaging.SingleEmailMessage> outboundEmails = new List<Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage outboundEmail = new Messaging.SingleEmailMessage();
        outboundEmail.setToAddresses(recipients);
        outboundEmail.setSaveAsActivity(false);
        outboundEmail.setUseSignature(true);
        outboundEmail.setSubject(subject);
        outboundEmail.setHtmlBody(body);

        outboundEmails.add(outboundEmail);
        System.debug(outboundEmails);
        Messaging.sendEmail(outboundEmails);
        System.debug('END sendEmails');
    }
}
