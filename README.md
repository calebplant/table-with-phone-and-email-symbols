# Datatable With Phone And Email Symbols

## Overview

A LWC that displays a datatable that converts Email and Phone fields to symbols. When clicked, the symbol shows the actual value in a popover. If the user clicks the popover email, they are able to send an email directly.

## Demo (~45 sec)

[Here's a link to a demo video for the component](https://www.youtube.com/watch?v=KOSM9LJVo1A)

## Some Screenshots

### Table with Email Popover

![Email Popover](media/table-with-email-popover.png)

### Sending an Email

![Sending an Email](media/send-email.png)